import requests
import json
import datetime


maasUrl = "https://mwn-maas.seln.wh.rnd.internal.ericsson.com"
params = 'program=ML66&testLoopType=REG1'
days = '60'


url = maasUrl + '/aggregated-summary?' + params + '&createdTimestamp=-' + days + '&limit=2000'
rawData = requests.get(url)

testRuns = json.loads(rawData.content)
testRuns.reverse()

alreadyBroken = False
timeForFix = 0
seriesList = []
totalTimeForFix = 0
dateValue = datetime.datetime.strptime(testRuns[000]['endTimestamp'], '%Y-%m-%dT%H:%M:%S.%fZ')

for testRun in testRuns:
    if testRun['total'] != testRun['passed'] and not alreadyBroken and testRun['endTimestamp']:
        brokenAt = datetime.datetime.strptime(testRun['endTimestamp'], '%Y-%m-%dT%H:%M:%S.%fZ') 
        alreadyBroken = True
    elif testRun['total'] == testRun['passed'] and alreadyBroken:
        alreadyBroken = False

        if testRun['endTimestamp']:
            fixedAt = datetime.datetime.strptime(testRun['endTimestamp'], '%Y-%m-%dT%H:%M:%S.%fZ')

            timeForFix = (fixedAt - brokenAt)/60 
    
            if str(brokenAt.date()) == str(dateValue.date()):
              totalTimeForFix += timeForFix.seconds
            else:
              if timeForFix:
                seriesList.append([str(brokenAt.day)+'.'+str(brokenAt.month), totalTimeForFix]) 
                totalTimeForFix = 0
                dateValue = brokenAt

jsonSeriesList = json.dumps(seriesList)
dataForBoard = '{"subtitle": "Trend about broken builds (60 days)", "description": "How log fix builds per day (minutes)", "series_list": [' + jsonSeriesList + ']}'
# print(dataForBoard)
data = {
  'tile': 'line_chart',
  'key': 'real_data_per_day',
  'data': dataForBoard
}

response = requests.post('http://localhost:7272/api/v0.1/2faec0892308434e8832057653bed78f/push', data=data)

print(response)