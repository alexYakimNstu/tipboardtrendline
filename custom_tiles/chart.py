import requests

data = {
  'tile': 'line_chart',
  'key': 'example_line',
  'data': '{"subtitle": "Trend about broken builds", "description": "How log fix builds per day", "series_list": [[["23.06", 150000], ["24.09", 125272], ["25.09", 190240], ["26.09", 229639], ["27.09", 240933], ["28.09", 260630], ["29.09", 108326]]]}'
}

response = requests.post('http://localhost:7272/api/v0.1/2faec0892308434e8832057653bed78f/push', data=data)

print(response)