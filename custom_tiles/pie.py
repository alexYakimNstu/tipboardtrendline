import requests

data = {
  'tile': 'pie_chart',
  'key': 'example_pie',
  'data': '{"title": "broken/worked build per day", "pie_data": [["broken build", 30], ["green build", 70]]}'
}

response = requests.post('http://localhost:7272/api/v0.1/2faec0892308434e8832057653bed78f/push', data=data)


print(response)