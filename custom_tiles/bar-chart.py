import requests

data = {
  'tile': 'advanced_plot',
  'key': 'bar_chart_test',
  'data': '{"title": "Metric Tons per Year", "description": "", "plotData": [[[2,1], [4,2], [6,3], [3,4]], [[5,1], [1,2], [3,3], [4,4]], [[4,1], [7,2], [1,3], [2,4]]]}'
}

response = requests.post('http://localhost:7272/api/v0.1/2faec0892308434e8832057653bed78f/push', data=data)

print(response)
